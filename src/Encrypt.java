

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

public class Encrypt {
	public static void main(String[] args) throws IOException {
		String key,keys,pl;
		int flag = 0, pll, pi1 = 0, pj1 = 0, pi2 = 0, pj2 = 0, g = 0;
		char a[] = new char[1000];
		char alphabets[][] = new char[1000][1000];
		char tempap[] = new char[1000];
		char tempap1[]=new char[1000];
		char plain[] = new char[100];
		int i, j, len, flag1 = 0, h = 0,lens;
		System.out.println("Enter key");				//ENTER THE KEY (use the same key in Decrypt)
		 File f = new File("C:\\Users\\Subash\\Desktop\\Cryptography\\chipertext.txt");  //PLACE YOUR  LOCATION FOR STORING CHIPER TEXT(After encryption your chiperTxt will be that location)
		 FileOutputStream fos = new FileOutputStream(f);
		 PrintWriter pw = new PrintWriter(fos);
		 BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		keys=br.readLine();										//GET THE KEY FROM USER
		lens = keys.length();
		char chs;
        key="";
         
        for(i=0; i<lens; i++)
        {
            chs = keys.charAt(i);
            if(chs!=' ')
                key = key + chs;
            keys = keys.replace(chs,' '); 
        }
        System.out.println("Check your placed location for chipertext");//chipertext.txt has CHIPERTEXT
 
       //System.out.println("Word after removing duplicate characters : " + key);
       len=key.length();
		for (i = 0; i < len; i++) {								//STORING KEY IN TEMP FOR CODE CONVINICE
			tempap[i] = key.charAt(i);
			tempap1[i]=tempap[i];
		}
		int ch=10;
		if(len<10)
		{
			ch=len;
		}
		for (i = 0; i < ch; i++) {								//STORING KEY IN 10X10 MATRIX IN ORDER
				alphabets[0][i] = key.charAt(i);
				h = i;											//FOR KNOWING LAST COLUMN POSITION" AFTER ENTERING KEY IN MATRIX
		}
		if (len > 10) {											//FOR STROING KEY IN 10X10 ORDERLY
			int m = len - 10;
			for (i = 0; i < m; i++) {
				alphabets[1][i] = key.charAt(i + 10);
				flag = 1;										//FOR KNOWING LAST ROW POSITION" AFTER ENTERING KEY IN MATRIX
				h = i;											//FOR KNOWING LAST COLUMN POSITION" AFTER ENTERING KEY IN MATRIX
			}
		}
		if (len > 20) {											//FOR STROING KEY IN 10X10 ORDERLY
			int m = len - 20;
			for (i = 0; i < m; i++) {
				alphabets[2][i] = key.charAt(i + 20);
				flag = 2;										//FOR KNOWING LAST ROW POSITION" AFTER ENTERING KEY IN MATRIX
				h = i;											//FOR KNOWING LAST COLUMN POSITION" AFTER ENTERING KEY IN MATRIX
			}
		}
		for (i = 65; i <= 91; i++) {							//FORMING A ARRAY WHICH CONTAINS ALPHABETS A TO Z
			a[g] = (char) i;
			g++;
		}
		g--;
		for(i=97;i<=123;i++)									//FORMING A ARRAY WHICH CONTAINS ALPHABETS a TO z
		{
			a[g]=(char) i;
			g++;
		}
		
		g--;
		for(i=33;i<=65;i++)										//FORMING A ARRAY WHICH CONTAINS OTHER CHARCTERS
		{
			a[g]=(char)i;
			g++;
		}
		g--;
		for(i=91;i<=97;i++)
		{
			a[g]=(char)i;
			g++;
		}
		g--;
		for(i=123;i<=127;i++)
		{
			a[g]=(char)i;
			g++;
		}
		g--;
		for(i=246;i<=252;i++)
		{
			a[g]=(char)i;
			g++;
		}
		int p = 0;
		for (i = flag; i < 10; i++) {							//"i = flag" USED FOR  GETTING CORRECT ROW IN MATRIX
			for (j = h + 1; j < 10; j++) {						//"j = h+1 " USED FOR  GETTING CORRECT COLUMN IN MATRIX
				while (p < 100) {									
					flag1 = 0;
					for (int k = 0; k < len; k++) {
						if (a[p] != tempap[k]) {				//CHECKING AND AVOID OVERWRITTING OF ALPABETS
							flag1++;							//USING THOSE VARIABLES FOR SOME INDICATION ,KNOWING ABOUT STATUS 
							}
						}
					if (flag1 == len) {
						alphabets[i][j] = a[p];
						p++;
						break;
					}
					p++;
				}												//WHILE LOOP CLOSED
			}													//2ND FOR LOOP CLOSED
			flag = 0;											//FOR SET ROW POSITION AS CORRECT 
			h = -1;												//FOR SET COLUMN POSITION AS CORRECT
		}														//1ST FOR LOOP CLOSED
		/*for (i = 0; i < 10; i++) {								//PRINTING REQURIED 10X10 MATRIX WHICH CONTAINS KEY VALUES FOLLOWED BY ALPHABETS
			for (j = 0; j < 10; j++) {
				System.out.print(alphabets[i][j] + "    ");
			}
			System.out.println();
		}*/
		Scanner sc2=null;
		try {
	        sc2 = new Scanner(new File("C:\\Users\\Subash\\Desktop\\Cryptography\\plaintext.txt"));//GETTING THE FILE WHICH SHOULD BE ENCRYPT(place your location)
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();  
	    }
		while (sc2.hasNextLine()) 								//CHECKING WHETHER INPUT HAS NEXT LINE
		{	
			Scanner s2 = new Scanner(sc2.nextLine());			//GET THE NEXT LINR INTO S2
	        while (s2.hasNext()) 								//CHECKING FOR NEXT ELEMENT 
	        {
	            String stxt = s2.next();						//GET THE NEXT ELEMENT INTO STXT
	            pl = stxt;
	            pll = pl.length();
	            for (i = 0; i < pll; i++) {						//STORING PLAIN TEXT CHARCHTERS IN PLAIN[] CHAR ARRAY
	            	plain[i] = pl.charAt(i);
	            }
	            if (pll % 2 != 0) {								//CHECKING WEATHER PLAIN TEXT CONATINS EVEN NO.OF OR ODD NO.OF CHAR
	            	plain[pll++] = 'X';							//PADDING 'X' IF IT CONTAINS ODD NO.OF CHAR
	            }
		int k = 0;
		p = 0;
		while (p < pll / 2) {									//FORMED "pll/2" FOR TAKING TWO CHAR AT A TIME
			for (i = 0; i < 10; i++) {
				for (j = 0; j < 10; j++) {
					if (plain[k] == alphabets[i][j]) {			//FINDING POSITIONS OF 1ST CHAR
							pi1 = i;
							pj1	= j;
						}
					if(plain[k]==plain[k+1])
					{
						plain[k+1]=(char)249;							//WHEN IT MEETS CONCSICATIVE SAME CHAR IT REPLACE 2ND CHAR AS "^" 
					}
					if (plain[k + 1] == alphabets[i][j]) {		//FINDING POSITIONS OF 2ND CHAR
						pi2 = i;
						pj2 = j;
					}
				}
			}													//1ST FOR CLOSED
			if (pi1 == pi2) {									//1ST METHOD TO FIND CIPHER TEXT
				if (pj1 != 9){							//FOR 1ST CHAR IN THAT PAIR
					 pw.write(alphabets[pi1][pj1 + 1]);

					//System.out.print(alphabets[pi1][pj1 + 1]); //PRINTING NEXT CHAR 
				}	
				else {
					pw.write(alphabets[pi1][0]);
					//System.out.print(alphabets[pi1][0]);
				}
				if (pj2 != 9) {	//FOR 2ND CHAR IN THAT PAIR
					pw.write(alphabets[pi2][pj2 + 1]);
					//System.out.print(alphabets[pi2][pj2 + 1]);  //PRINTING NEXT CHAR
				} else {
					pw.write(alphabets[pi2][0]);
					//System.out.print(alphabets[pi2][0]);
				}
			} 													//IF CLOSED
			else if (pj1 == pj2) {								//2ND METHOD TO FIND CIPHER TEXT
				if (pi1 != 9) {									//FOR 1ST CHAR IN THAT PAIR
					pw.write(alphabets[pi1 + 1][pj1]);
					//System.out.print(alphabets[pi1 + 1][pj1]);
				} else {
					pw.write(alphabets[0][pj1]);
					//System.out.print(alphabets[0][pj1]);
				}
				if (pi2 != 9) {									//FOR 2ND CHAR IN THAT PAIR
					pw.write(alphabets[pi2 + 1][pj2]);
					//System.out.print(alphabets[pi2 + 1][pj2]);
				} else {
					pw.write(alphabets[0][pj2]);
					//System.out.print(alphabets[0][pj2]);
				}
			}													//ELSE IF CLOSED
			else {	//3RD METHOD TO FIND CIPHER TEXT
				pw.write(alphabets[pi1][pj2]);
				//System.out.print(alphabets[pi1][pj2]);
				pw.write(alphabets[pi2][pj1]);
				//System.out.print(alphabets[pi2][pj1]);
			}
			p++;
			k = k + 2;											//INCREMENT +2 FOR TAKING TWO CHAR				
		}	
		pw.write(" ");
	        }
	      pw.write("\n");
	    }
		pw.flush();
		fos.close();
		pw.close();

	}
	 


}
